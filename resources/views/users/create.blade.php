@extends('templates.home')
@section('title')
    Create User
@endsection
@section('content')
    <div class="container" >
      <br><br>
        <h3>Create User</h3>
        <hr>
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('status') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5> Create a New User</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">

                  {{-- @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                  @endif --}}

                    <form action="{{ route('users.store') }}" class="form-group" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="name" >Username</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" type="text" name="name" id="name" value="{{ old('name') }}">
                                {{ ($errors->has('name')) ? $errors->first('name') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="email">Email</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="email" id="email" type="email" value="{{ old('email') }}">
                                {{ ($errors->has('email')) ? $errors->first('email') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="password">password</label>
                            </div>
                            <div class="col-md-8">
                                <input type="password" class="form-control" name="password" id="password" type="password" value="{{ old('password') }}">
                                {{ ($errors->has('password')) ? $errors->first('password') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="password_confirmation">password_confirmation</label>
                            </div>
                            <div class="col-md-8">
                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" type="password" >
                                {{ ($errors->has('password_confirmation')) ? $errors->first('password_confirmation') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="address">address</label>
                            </div>
                            <div class="col-md-8">
                                <textarea name="address" class="form-control" id="address" cols="20" rows="5"></textarea>
                                {{ ($errors->has('address')) ? $errors->first('address') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="phone">phone</label>
                            </div>
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="phone" id="phone">
                                {{ ($errors->has('phone')) ? $errors->first('phone') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="input-group mb-3">
                                <div class="col-md-3"> Avatar</div>
                            </div>
                            <div class="col-md-8">
                                <div class="custom-file">
                                    <label for="avatar" class="custom-file-label">Upload Avatar</label>
                                    <input type="file" class="custom-file-input" name="avatar" id="avatar" placeholder="avatar">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
