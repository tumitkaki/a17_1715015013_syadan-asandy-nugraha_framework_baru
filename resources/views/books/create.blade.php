@extends('templates.home')
@section('title')
    Create Books
@endsection
@section('content')
    <div class="container" >
      <br><br>
        <h3>Create Books</h3>
        <hr>
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5> Create a New Books</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('books.store') }}" class="form-group" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="title" >Title</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="title" id="title">
                                {{ ($errors->has('title')) ? $errors->first('title') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="author">Author</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="author" id="author">
                                {{ ($errors->has('author')) ? $errors->first('author') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="publisher">Publisher</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="publisher" id="publisher">
<<<<<<< HEAD
=======
                                {{ ($errors->has('publisher')) ? $errors->first('publisher') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="price">Price</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="price" id="price">
                                {{ ($errors->has('price')) ? $errors->first('price') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="stock">Stock</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="stock" id="stock">
                                {{ ($errors->has('stock')) ? $errors->first('stock') : "" }}
>>>>>>> SUBMISSION_3
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="deskripsi">Deskripsi</label>
                            </div>
                            <div class="col-md-8">
<<<<<<< HEAD
                                <textarea class="form-control" name="deskripsi" id="deskripsi" rows="6" >
                                </textarea>
=======
                                <textarea class="form-control" name="description" id="deskripsi" rows="6" >

                                </textarea>
                                {{ ($errors->has('description')) ? $errors->first('description') : "" }}
>>>>>>> SUBMISSION_3
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="category">Category</label>
                            </div>
                            <div class="col-md-8">
                                <select multiple='multiple' name="category[]" id="category" class="form-control {{$errors->first('category') ? "is-invalid": ""}}">
                                    <option value="">Select Category</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->cat_name }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    {{$errors->first('category')}}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
              						<div class="input-group mb-3">
              							<div class="col-md-3 text-primary">
              								Cover
              							</div>

              							<div class="col-md-8">
              								<div class="custom-file">
                									<input type="file" class="custom-file-input" name="cover" id="newAvatar">
                									<label class="custom-file-label" for="newAvatar">Upload Cover</label>
              									  {{ ($errors->has('cover')) ? $errors->first('cover') : "" }}
              								</div>
              							</div>
              						</div>
              					</div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
