@extends('templates.home')
@section('title')
    Detail of Book
@endsection
@section('content')
    <h1>Detail of Book </h1>
    <hr>
    <br>
    <div class="card bg-white border-info" style="max-width:70%; margin:auto; min-height:400px;">
        <div class="row " style="padding:25px">
            <div class="col-md-2 offset-md-5 offset-sm-4">
                <img src="{{ asset('storage/'.$book['cover']) }}" style="height:150px; width:150px; color:black;" class="rounded-circle" alt="img">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>{{ $book['title'] }}</h3>
            </div>
        </div>
        <hr>
        <br>
        <div class="row">
            <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                Author
            </div>
            <div class="col-md-4 col-sm-4">
                {{ $book['author'] }}
            </div>
            <br>
        </div>
        <div class="row">
            <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                Publisher
            </div>
            <div class="col-md-4 col-sm-4">
                {{ $book['publisher'] }}
            </div>
            <br>
        </div>
        <div class="row">
            <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
              Price
            </div>
            <div class="col-md-4 col-sm-4 ">
                {{ $book['price'] }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
              Stock
            </div>
            <div class="col-md-4 col-sm-4 ">
                {{ $book['stock'] }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
              Description
            </div>
            <div class="col-md-4 col-sm-4 ">
                {{ $book['description'] }}
            </div>
        </div>
        <div class="row">
    			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
    				Category
    			</div>
    			<div class="col-md-4 col-sm-4">
    				<ul>
    				@foreach ($book->category as $categories)
    					   <li>{{ $categories->cat_name }}</li>
    				@endforeach
    				</ul>
    			</div>
    		</div>
    </div>
@endsection
