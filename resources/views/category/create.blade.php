@extends('templates.home')
@section('title')
    Create Category
@endsection
@section('content')
    <div class="container" ><br><br>
        <h3>Create Category</h3>
        <hr>
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('status') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5> Create a New Category</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('categories.store') }}" class="form-group" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="cat_name" >Category Name</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="cat_name" id="cat_name">
                                {{ ($errors->has('cat_name')) ? $errors->first('cat_name') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="desc" class="text-primary">Description</label>
                            </div>
                            <div class="col-md-8">
                                <textarea name="desc" id="desc" rows="5" cols="58"></textarea>
                                {{ ($errors->has('desc')) ? $errors->first('desc') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
