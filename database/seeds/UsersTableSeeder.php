<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'indah',
                'email'=>'indah@gmail.com',
                'address' => 'jl pramuka indah',
                'phone'=>'012389712',
                'status'=>'ACTIVE',
                'avatar'=>'avatars/1.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'rami',
                'email'=>'rami@gmail.com',
                'address' => 'jl pramuka rami',
                'phone'=>'023389712',
                'status'=>'ACTIVE',
                'avatar'=>'avatars/2.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'jihyo',
                'email'=>'jihyo@gmail.com',
                'address' => 'jl pramuka jihyo',
                'phone'=>'0123838462',
                'status'=>'INACTIVE',
                'avatar'=>'avatars/3.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'yerin',
                'email'=>'yerin@gmail.com',
                'address' => 'jl pramuka yerin',
                'phone'=>'02823479712',
                'status'=>'ACTIVE',
                'avatar'=>'avatars/4.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'nat',
                'email'=>'nat@gmail.com',
                'address' => 'jl pramuka nat',
                'phone'=>'0123485462',
                'status'=>'INACTIVE',
                'avatar'=>'avatars/5.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'roy',
                'email'=>'roy@gmail.com',
                'address' => 'jl pramuka roy',
                'phone'=>'03767826123',
                'status'=>'INACTIVE',
                'avatar'=>'avatars/6.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'ray',
                'email'=>'ray@gmail.com',
                'address' => 'jl pramuka ray',
                'phone'=>'0827316234',
                'status'=>'ACTIVE',
                'avatar'=>'avatars/7.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'rizuki',
                'email'=>'rizuki@gmail.com',
                'address' => 'muara wahau',
                'phone'=>'08912736721',
                'status'=>'ACTIVE',
                'avatar'=>'avatars/8.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'agung',
                'email'=>'agung@gmail.com',
                'address' => 'kongbeng',
                'phone'=>'01098387216',
                'status'=>'ACTIVE',
                'avatar'=>'avatars/9.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'mufli',
                'email'=>'mufli@gmail.com',
                'address' => 'gangbang',
                'phone'=>'08938261736',
                'status'=>'ACTIVE',
                'avatar'=>'avatars/10.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'hanie',
                'email'=>'hanie@gmail.com',
                'address' => 'Sambutan',
                'phone'=>'08386128367',
                'status'=>'ACTIVE',
                'avatar'=>'avatars/11.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'zulkipil',
                'email'=>'zul@gmail.com',
                'address' => 'penajam',
                'phone'=>'09898712836',
                'status'=>'INACTIVE',
                'avatar'=>'avatars/12.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'abduh',
                'email'=>'abduh@gmail.com',
                'address' => 'berau',
                'phone'=>'08378293701',
                'status'=>'INACTIVE',
                'avatar'=>'avatars/13.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'astrid',
                'email'=>'astrid@gmail.com',
                'address' => 'loa janan',
                'phone'=>'09281932932',
                'status'=>'ACTIVE',
                'avatar'=>'avatars/14.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'name' => 'luthfi',
                'email'=>'lupi@gmail.com',
                'address' => 'sanga sanga',
                'phone'=>'08478647326',
                'status'=>'INACTIVE',
                'avatar'=>'avatars/15.jpg',
                'password' => Hash::make('123456')
            ],
        ]);
    }
}
