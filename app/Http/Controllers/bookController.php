<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Category;
use App\Books_Categories;

class bookController extends Controller
{
    public function index()
    {
        $books = Book::paginate(5);
        return view('books.index', compact('books'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('books.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
          'title' => 'required',
          'description' => 'required|max:200',
          'author' => 'required',
          'publisher' => 'required',
          'price' => 'required|numeric|max:999999',
          'stock' => 'required|numeric',
          'category'=> 'required',
          'cover' => 'image',
        ]);

        $new_book = new Book();
        $new_book->title = $request->get('title');
        $new_book->description = $request->get('description');
        $new_book->author = $request->get('author');
        $new_book->publisher = $request->get('publisher');
        $new_book->price = $request->get('price');
        $new_book->stock = $request->get('stock');
        if ($request->file('cover')) {
            $file = $request->file('cover')->store('cover','public');
            $new_book->cover = $file;
        }
        $new_book->save();

        $new_book->category()->attach($request->get('category'));

        return redirect()->route('books.index')->with('status','Book succesfully Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::findOrFail($id);
        return view('books.show',['book'=>$book]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);
        $categories = Category::all();
        return view('books.edit',['book'=>$book, 'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'title' => 'required',
          'description' => 'required|max:200',
          'author' => 'required',
          'publisher' => 'required',
          'price' => 'required|numeric|max:999999',
          'stock' => 'required|numeric',
          'category'=> 'required',
        ]);

        $update_book = Book::findOrFail($id);
        $update_book->title = $request->get('title');
        $update_book->description = $request->get('description');
        $update_book->author = $request->get('author');
        $update_book->publisher = $request->get('publisher');
        $update_book->price = $request->get('price');
        $update_book->stock = $request->get('stock');
        if ($request->file('cover')) {
            if ($update_book->cover && file_exists(storage_path('storage/'.$update_book->cover))) {
                Storage::delete('storage/'.$update_book->cover);
            }
            $file = $request->file('cover')->store('cover','public');
            $update_book->cover = $file;
        }
        $update_book->save();

        $update_book->category()->sync($request->get('category'));

        return redirect()->route('books.index',['id'=>$id])->with('status','Book succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $book = Book::findOrFail($id);
         if ($book->cover && file_exists(storage_path('public/storage/'.$book->cover))) {
             Storage::delete('public/storage/'.$book->cover);
         }
         $book->delete();
         return redirect()->route('books.index')->with('status', 'Book Successfully deleted');
     }

     public function search(Request $request)
     {
         $books = Book::when($request->found, function ($query) use ($request) {
             $query->where('title', 'like', "%{$request->found}%");
             // orWhere('description', 'like', "%{$request->found}%");
         })->paginate(5);

         $books->appends($request->only('found'));

         return view('books.index', compact('books'));
     }
}
