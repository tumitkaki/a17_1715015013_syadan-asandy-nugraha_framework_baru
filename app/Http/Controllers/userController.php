<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class userController extends Controller
{
    public function index()
    {
        $users = User::paginate(5);
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:5|max:25|unique:users',
            'address' => 'required|max:100',
            'email' => 'required|email|unique:users',
            'phone' => 'required',
            'avatar' => 'required|image',
            'password' => 'required|min:8|max:32',
            'password_confirmation'=> 'required|same:password'
        ]);
        $new_user = new User();
        $new_user->name = $request->get('name');
        $new_user->address = $request->get('address');
        $new_user->email = $request->get('email');
        $new_user->phone = $request->get('phone');
        $new_user->status = "ACTIVE";
        $new_user->password = Hash::make($request->get('password'));
        if ($request->file('avatar')) {
            $file = $request->file('avatar')->store('avatars','public');
            $new_user->avatar = $file;
        }
        $new_user->save();
        return redirect()->route('users.index')->with('status','User Succesfully Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $user = User::findOrFail($id);
         return view('users.show',['user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $user = User::findOrFail($id);
         return view('users.edit',['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'email' => 'required|email',
          'address' => 'required|max:100',
          'phone' => 'required|numeric',
          'avatar' => 'image'
        ]);

        $update_user = User::findOrFail($id);
        $update_user->email = $request->get('email');
        $update_user->address = $request->get('address');
        $update_user->phone = $request->get('phone');
        $update_user->status = $request->get('status');
        if ($request->file('avatar')) {
            if ($update_user->avatar && file_exists(storage_path('public/storage/'.$update_user->avatar))) {
                Storage::delete('public/storage/'.$update_user->avatar);
            }
            $file = $request->file('avatar')->store('avatars','public');
            $update_user->avatar = $file;
        }
        $update_user->save();
        return redirect()->route('users.index',['id'=>$id])->with('status','User succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->avatar && file_exists(storage_path('public/storage/'.$user->avatar))) {
            Storage::delete('public/storage/'.$user->avatar);
        }
        $user->delete();
        return redirect()->route('users.index')->with('status', 'User Successfully deleted');
    }

    public function search(Request $request)
    {
        // $found = $request->found;
        //
        // $users = User::where('email','LIKE', '%'.$found.'%')->paginate();
        // return view('users.index', compact('users'));
        $users = User::when($request->found, function ($query) use ($request) {
            $query->where('email', 'like', "%{$request->found}%");
        })->paginate(5);
        return view('users.index', compact('users'));
    }
}
