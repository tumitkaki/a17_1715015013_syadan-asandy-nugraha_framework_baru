<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $categories = Category::paginate(5);
        return view('category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
<<<<<<< HEAD
      echo "Anda Berada pada method Store pada categoryController ";
      return $request;
=======
        $request->validate([
            'cat_name' => 'required|unique:categories',
            'desc' => 'required|max:200',
        ]);
        $new_cat = new Category();
        $new_cat->cat_name = $request->get('cat_name');
        $new_cat->desc = $request->get('desc');
        $new_cat->save();
        return redirect()->route('categories.index')->with('status','Category Succesfully Created');
>>>>>>> SUBMISSION_3
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);
        return view('category.show',['category'=>$category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('category.edit',['category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'cat_name' => 'required',
            'desc' => 'required|max:200',
        ]);
        $update_cat = Category::findOrFail($id);
        $update_cat->cat_name = $request->get('cat_name');
        $update_cat->desc = $request->get('desc');
        $update_cat->save();
        return redirect()->route('categories.index',['id'=>$id])->with('status','Category succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $category = Category::findOrFail($id);
         $category->delete();
         return redirect()->route('categories.index')->with('status', 'Category Successfully deleted');
     }

     public function search(Request $request)
     {

         $categories = Category::when($request->found, function ($query) use ($request) {
            $query->where('cat_name', 'like', "%{$request->found}%");
         })->paginate(5);
         return view('category.index', compact('categories'));
     }
}
