<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Order extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id', 'tot_har', 'inv_no', 'status'
    ];

    public function users() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
